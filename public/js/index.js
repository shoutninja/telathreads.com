var publishableKey = 'pk_test_H9PXgi4Q4WI7lnkZKCn8wtwk';

var app = angular.module('app', []);

app.service('productService', function() {
    return {
        addProduct: function(newProduct) {
            var a = JSON.parse(localStorage.getItem('deployshop-cart-products'));
            if (a) {
                a.push(newProduct);
                localStorage.setItem('deployshop-cart-products', JSON.stringify(a));
            } else {
                var arr = [];
                arr.push(newProduct);
                localStorage.setItem('deployshop-cart-products', JSON.stringify(arr));
            }
        },
        getProducts: function() {
            return JSON.parse(localStorage.getItem('deployshop-cart-products'));
        },
        removeProduct: function(product) {
            var products = JSON.parse(localStorage.getItem('deployshop-cart-products'));
            for (var i = 0; i < products.length; i++) {
                if (products[i].id == product.id) {
                    products.splice(i, 1);
                    localStorage.setItem('deployshop-cart-products', JSON.stringify(products));
                    return products;
                }
            }
        },
        totalPrice: function() {
            var products = JSON.parse(localStorage.getItem('deployshop-cart-products'));
            console.log(products);
            var totalPrice = 0;
            products.forEach(function(product) {
                totalPrice += parseFloat(product.price);
            });
            return totalPrice;
        },
        removeAllProducts: function() {
            localStorage.removeItem('deployshop-cart-products');
        }
    };
})

app.controller('HomeCtrl', ['$scope', '$http', function($scope, $http) {
    $scope.change = function(product) {
        window.location = '/product/' + product.id;
    }
}]);

app.controller('ProductCtrl', ['$scope', '$http', 'productService', function($scope, $http, productService) {
    $scope.addToCart = function(product) {
        if (product.sizes) {
            if (product.size) {
                productService.addProduct(product);
                window.location = '/cart';
            }
        } else {
            productService.addProduct(product);
            window.location = '/cart';
        }
    };
    $scope.addCount = function() {
        $scope.count++;
        if ($scope.count == $scope.product.photos.length) {
            $scope.count = 0;
        }
        console.log($scope.count);
    }
}]);

app.controller('CartCtrl', ['$scope', '$http', 'productService', function($scope, $http, productService) {
    var handler = StripeCheckout.configure({
        key: publishableKey,
        locale: 'auto',
        token: function(token) {
            var data = {
                items: items,
                token: token
            }
            $http.post('/api/charge', data)
                .success(function(response) {
                    if (response.substring(0, 7) == '/order/') {
                        productService.removeAllProducts();
                        window.location = response;
                    }
                });
        }
    });
    $scope.products = productService.getProducts();
    $scope.totalPrice = 0;
    if ($scope.products) {
        $scope.totalPrice = productService.totalPrice();
        var items = [];
        $scope.products.forEach(function(product) {

            if (product.sizes) {
                items.push({
                    id: product.id,
                    size: product.size
                });
            } else {
                items.push({
                    id: product.id
                });
            }
            console.log(items);
        });
    }
    $scope.removeProduct = function(product) {
        $scope.products = productService.removeProduct(product);
        $scope.totalPrice = productService.totalPrice();
    }
    $scope.checkout = function() {
        handler.open({
            name: 'Tela Threads',
            description: $scope.products.length + " Items",
            amount: $scope.totalPrice * 100,
            shippingAddress: true
        });
    }
}]);

app.controller('AdminCtrl', ['$scope', '$http', function($scope, $http) {
    $scope.addProduct = function(product) {
        var data = {
            name: product.name,
            desc: product.desc,
            price: product.price,
            photos: product.photos,
            sizes: product.sizes,
            unlisted: product.unlisted
        }
        $http.post('/api/newProduct', data)
            .success(function(data) {
                console.log(data);
                product.name = "";
                product.desc = "";
                product.price = "";
                product.photos = "";
            });
    }
    $scope.shipOrder = function(order) {
        var data = {
            id: order._id
        }
        console.log('ayy');
        $http.post('/api/shipOrder', data)
            .success(function(data) {
                console.log(data);
            });
    }
    $scope.removeProduct = function(product) {
        var data = {
            id: product.id
        }
        $http.post('/api/removeProduct', data)
            .success(function(data) {
                console.log(data);
            });
    }
    $scope.updateProductClick = function(product) {
        console.log(product);
        $scope.newproduct = product;
        if ($scope.newproduct.photos.join) {
            $scope.newproduct.photos = $scope.newproduct.photos.join(',');
        }
        $scope.newproduct.price = parseInt($scope.newproduct.price);
    }
    $scope.updateProduct = function(newproduct) {
        var data = {
            id: newproduct._id,
            name: newproduct.name,
            desc: newproduct.desc,
            price: newproduct.price,
            photos: newproduct.photos,
            sizes: newproduct.sizes,
            unlisted: newproduct.unlisted
        }
        console.log(data);
        $http.post('/api/updateProduct', data)
            .success(function(data) {
                console.log(data);
            });
    }
}]);
