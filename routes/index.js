var express = require('express');
var fs = require('fs');
var bcrypt = require('bcryptjs');
var path = require('path');
var mongoose = require('mongoose');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var router = express.Router();
var obj = JSON.parse(fs.readFileSync(path.join(__dirname, '../config.js'), 'utf8'));
console.log(obj.DeployShopPublishableKey);

fs.readFile('public/js/index.js', 'utf8', function(err, data) {
    if (err) return console.log(err);
    if (data.indexOf('KEYTHATNEEDSTOBEREPLACED') > -1) {
        data = data.replace('KEYTHATNEEDSTOBEREPLACED', obj.DeployShopPublishableKey);
        fs.writeFile('public/js/index.js', data, 'utf8', function(err) {
            if (err) return console.log(err);
        });
    }
});

var stripe = require("stripe")(obj.DeployShopSecretKey);

var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'telathreads@gmail.com',
        pass: 'TelaThreads!420'
    }
});

var password = '';

var conn = mongoose.createConnection('mongodb://localhost/deploy-tela-v1');

var ProductSchema = new mongoose.Schema({
    id: String,
    name: String,
    desc: String,
    price: String,
    photos: [String],
    sizes: Boolean,
    unlisted: {
        type: Boolean,
        required: false
    }
});

var OrderSchema = new mongoose.Schema({
    email: String,
    products: [{
        id: String,
        size: String
    }],
    name: String,
    address_city: String,
    addres_country: String,
    address_line1: String,
    address_line2: String,
    address_state: String,
    address_zip: String
});

var Product = conn.model('Product', ProductSchema);
var Order = conn.model('Order', OrderSchema);

router.get('/', function(req, res, next) {
    Product.find({}, function(err, products) {
        if (products) {
            res.render('index', {
                title: obj.DeployShopName,
                products: products
            });
        } else {
            res.render('index', {
                title: obj.DeployShopName
            });
        }
    });
});

router.get('/about', function(req, res) {
    res.render('about', {
        title: 'About'
    })
})

router.get('/login', function(req, res) {
    res.render('login', {
        title: 'Login'
    });
});

router.get('/product/:productID', function(req, res) {
    Product.findOne({
        id: req.params.productID
    }, function(err, product) {
        if (product) {
            res.render('product', {
                title: product.name,
                product: product
            });
        } else {
            res.redirect('/');
        }
    })
});

router.get('/order/:orderID', function(req, res) {
    Order.findOne({
        _id: req.params.orderID
    }, function(err, order) {
        if (order) {
            res.render('order', {
                title: order._id,
                order: order
            });
        } else {
            res.redirect('/');
        }
    })
});

router.get('/cart', function(req, res) {
    res.render('cart', {
        title: obj.DeployShopName + ' - Cart'
    });
});

router.post('/login', function(req, res) {
    var username = req.body.username;
    password = req.body.password;
    bcrypt.compare(password, bcrypt.hashSync(obj.DeployShopPassword, 10), function(err, response) {
        if (response == true) {
            console.log('ayyyyyy');
            Product.find({}, function(err, products) {
                if (products) {
                    Order.find({}, function(err, orders) {
                        if (orders) {
                            console.log(orders);
                            res.render('admin', {
                                products: products,
                                orders: orders
                            });
                        } else {
                            res.render('admin', {
                                products: products
                            });
                        }
                    });
                } else {
                    res.render('admin');
                }
            });
        } else {
            console.log('ohhh');
        }
    });
});

router.post('/api/newProduct', function(req, res) {
    console.log(req.body);
    bcrypt.compare(password, bcrypt.hashSync(obj.DeployShopPassword, 10), function(err, response) {
        if (response == true) {
            var makeText = function() {
                var text = "";
                var possible = "abcdefghijklmnopqrstuvwxyz";

                for (var i = 0; i < 3; i++) {
                    text += possible.charAt(Math.floor(Math.random() * possible.length));
                }
                return text;
            }
            var text = makeText();
            var checkText = function() {
                Product.findOne({
                    id: text
                }, function(err, product) {
                    if (product) {
                        text = makeText();
                        checkText();
                    } else {
                        console.log('new product');
                        var photos = req.body.photos.split(',');
                        var newProduct = Product({
                            name: req.body.name,
                            desc: req.body.desc,
                            price: req.body.price,
                            photos: photos,
                            id: text,
                            sizes: req.body.sizes,
                            unlisted: req.body.unlisted
                        });

                        newProduct.save(function(err, product) {
                            if (err) return console.log(err);
                            console.log(product);
                            res.send(product);
                        });
                    }
                });
            }
            checkText();
        } else {
            res.send('You do not have access');
        }
    });
});

router.post('/api/removeProduct', function(req, res) {
    bcrypt.compare(password, bcrypt.hashSync(obj.DeployShopPassword, 10), function(err, response) {
        if (response == true) {
            Product.findOneAndRemove({
                id: req.body.id
            }, function(err) {
                if (!err) {
                    res.send('good!');
                }
            });
        } else {
            res.send('You do not have access');
        }
    });
});

router.post('/api/shipOrder', function(req, res) {
    bcrypt.compare(password, bcrypt.hashSync(obj.DeployShopPassword, 10), function(err, response) {
        if (response == true) {
            Order.findOne({
                _id: req.body.id
            }, function(err, order) {
                console.log(order);
                transporter.sendMail({
                    from: "Tela Threads Team <telathreads@gmail.com>",
                    to: order.email,
                    subject: "Your order has shipped!",
                    html: "<h2>We are informing you that your order has shipped!</h2> If you need any help, reach out to us at <a href='mailto:info@telathreads.com'>info@telathreads.com</a>."
                }, function(error, response) {
                    if (error) {
                        console.log(error);
                    } else {
                        console.log("Message sent: " + response);
                        Order.findOneAndRemove({
                            _id: req.body.id
                        }, function(err) {});
                    }
                });
            });
        } else {
            res.send('You do not have access');
        }
    });
});

router.post('/api/updateProduct', function(req, res) {
    bcrypt.compare(password, bcrypt.hashSync(obj.DeployShopPassword, 10), function(err, response) {
        if (response == true) {
            Product.findOne({
                _id: req.body.id
            }, function(err, product) {
                console.log(product);
                console.log(req.body.photos);
                product.name = req.body.name;
                product.desc = req.body.desc;
                product.price = req.body.price;
                product.photos = req.body.photos.split(',');
                product.sizes = req.body.sizes;
                product.unlisted = req.body.unlisted;

                product.save(function(err, product) {
                    if (err) return console.log(err);
                    console.log(product);
                    res.send(product);
                });
            });
        } else {
            res.send('You do not have access');
        }
    });
});

router.post('/api/charge', function(req, res) {
    var stripeToken = req.body.token.id;
    var email = req.body.token.email;
    var items = req.body.items;
    price = 0;
    for (var i = 0; i < items.length; i++) {
        Product.findOne({
            id: items[i].id
        }, function(err, product) {
            if (product) {
                price += parseFloat(product.price);
            }
            if (i == items.length) {
                console.log('ayy');
                chargeCard(price, stripeToken, items, email);
            }
        });
    }


    var chargeCard = function(totalPrice, stripeToken, items, email) {
        console.log(stripeToken);
        console.log(totalPrice);
        var charge = stripe.charges.create({
            amount: totalPrice * 100,
            currency: "usd",
            source: stripeToken,
            description: 'Charge for Trendup',
            receipt_email: email
        }, function(err, charge) {
            console.log(charge);
            if (!err) {
                console.log(items);
                var newOrder = new Order({
                    email: email,
                    products: items,
                    name: charge.source.name,
                    address_city: charge.source.address_city,
                    addres_country: charge.source.addres_country,
                    address_line1: charge.source.address_line1,
                    address_line2: charge.source.address_line2,
                    address_state: charge.source.address_state,
                    address_zip: charge.source.address_zip,
                });
                newOrder.save(function(err, order) {
                    if (err) return console.log(err);
                    console.log(order);
                    transporter.sendMail({
                        from: "Tela Threads Team <telathreads@gmail.com>",
                        to: 'info@telathreads.com,henry@telathreads.com,7205562453@vtext.com',
                        subject: "New Order!",
                        html: "Check the orders! You made $" + totalPrice + ". <a href='https://www.telathreads.com/login'>/login</a>."
                    }, function(error, response) {
                        if (error) {
                            console.log(error);
                        } else {
                            console.log("Message sent: " + response);
                            Order.findOneAndRemove({
                                _id: req.body.id
                            }, function(err) {});
                        }
                    });

                    res.send('/order/' + order._id)
                });
            }
            if (err && err.type === 'StripeCardError') {
                console.log(err);
                res.send('error');
            }
        });
    }

});

module.exports = router;
